package jp.alhinc.nishimura_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> salesMap = new HashMap<>();
		BufferedReader br = null;
		// branch.lstの読み込み等処理
		try {
			File branchlstFile = new File(args[0], "branch.lst");
			if (!branchlstFile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			br = new BufferedReader(new FileReader(branchlstFile));
			String branchlstLine;
			// branch.lstを一行ずつ読み込む
			while ((branchlstLine = br.readLine()) != null) {
				String[] data = branchlstLine.split(",", -1);
				if (!data[0].matches("[0-9]{3}") || data.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchMap.put(data[0], data[1]);
				salesMap.put(data[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		// rcdファイルの読み込み等処理
		File directory = new File(args[0]);
        String[] filenames = directory.list();
		List<String> rcdFilenames = new ArrayList<>();

		for (String filename : filenames) {
			if (file.matches("^\\d{8}.rcd$")) {
                rcdFilenames.add(filename);
            }
        }

        Collections.sort(rcdFilenames);

        int min = Integer.parseInt(rcdFilenames.get(0));
        int max = Integer.parseInt(rcdFilenames.get(rcdFilenames.size() - 1));

		if (max - min != rcdFilenames.size() - 1) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
        }

		for (String filename : rcdFilenames) {
            File rcdFile = new File(args[0], filename);
            try {
                br = new BufferedReader(new FileReader(rcdFile));
                String shopCode = br.readLine();
                long shopSales = Long.parseLong(br.readLine());
                String thirdLine = br.readLine();
                if (thirdLine != null) {
                    System.out.println(filename + "のフォーマットが不正です");
                    return;
                }
                if (branchMap.containsKey(shopCode)) {
                    System.out.println(filename + "の支店コードが不正です");
                    return;
                }
                salesMap.put(shopCode, salesMap.get(shopCode) + shopSales);
                // コンソールで処理確認するためのコード
                // System.out.println(shopCode + "," + map.get(shopCode) +
                // "," + map1.get(shopCode));
                if (salesMap.get(shopCode).toString().length() > 10) {
                    System.out.println("合計金額が10桁を超えました");
                    return;
                }
            } catch (IOException e) {
                System.out.println("予期せぬエラーが発生しました");
                return;
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        System.out.println("予期せぬエラーが発生しました");
                        return;
                    }
                }
            }
		}
		// branch.outの書き込み処理
		File branchOutFile = new File(args[0], "branch.out");
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(branchOutFile));
			for (Map.Entry<String, String> entry : branchMap.entrySet()) {
				String code = entry.getKey();
				bw.write(code + "," + entry.getValue() + "," + salesMap.get(code) + "\r\n");
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}